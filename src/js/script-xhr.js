/**
 * Affiche la carte d'un pokémon passé en paramètre
 * 
 * @param nameOrIndex nom ou index du pokémon à afficher
 */
function addPokemon(nameOrIndex) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `${ROOT_URL}/pokemon/${nameOrIndex}/`, true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            const data = JSON.parse(xhr.responseText);
            addPokemonCard(data);
        } else {
            console.log(Error(xhr.statusText));
        }
    }
    xhr.send();
}


/**
 * Affiche les cartes correspondant à chacune des espèces apparaissant dans la chaîne d'évolution de l'espèce passée en paramètre
 * 
 * @param nameOrIndex espèce de départ de la chaîne d'évolution
 */
function addEvolutionChain(nameOrIndex) {
    // à compléter
}
